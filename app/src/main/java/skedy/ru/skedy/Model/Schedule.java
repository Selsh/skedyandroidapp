package skedy.ru.skedy.Model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "schedule")
public class Schedule {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("hall_number")
    @Expose
    private String hallNumber;
    @SerializedName("dayOfTheWeak")
    @Expose
    private String dayOfTheWeak;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("teacher")
    @Expose
    private String teacher;
    @SerializedName("lessonType")
    @Expose
    private String lessonType;
    @SerializedName("lessonName")
    @Expose
    private String lessonName;

    public Schedule() {
    }

    @Ignore
    public Schedule(String hallNumber, String dayOfTheWeak, String time, String teacher, String lessonType, String lessonName) {
        this.hallNumber = hallNumber;
        this.dayOfTheWeak = dayOfTheWeak;
        this.time = time;
        this.teacher = teacher;
        this.lessonType = lessonType;
        this.lessonName = lessonName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHallNumber() {
        return hallNumber;
    }

    public void setHallNumber(String hallNumber) {
        this.hallNumber = hallNumber;
    }

    public Schedule withHallNumber(String hallNumber) {
        this.hallNumber = hallNumber;
        return this;
    }

    public String getDayOfTheWeak() {
        return dayOfTheWeak;
    }

    public void setDayOfTheWeak(String dayOfTheWeak) {
        this.dayOfTheWeak = dayOfTheWeak;
    }

    public Schedule withDayOfTheWeak(String dayOfTheWeak) {
        this.dayOfTheWeak = dayOfTheWeak;
        return this;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Schedule withTime(String time) {
        this.time = time;
        return this;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Schedule withTeacher(String teacher) {
        this.teacher = teacher;
        return this;
    }

    public String getLessonType() {
        return lessonType;
    }

    public void setLessonType(String lessonType) {
        this.lessonType = lessonType;
    }

    public Schedule withLessonType(String lessonType) {
        this.lessonType = lessonType;
        return this;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public Schedule withLessonName(String lessonName) {
        this.lessonName = lessonName;
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
