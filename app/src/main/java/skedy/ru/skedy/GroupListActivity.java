package skedy.ru.skedy;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Response;
import skedy.ru.skedy.Api.ApiFactory;
import skedy.ru.skedy.Api.SkedyApi;
import skedy.ru.skedy.DB.AppDatabase;
import skedy.ru.skedy.Model.Groups;
import skedy.ru.skedy.Model.GroupsOnDB;
import skedy.ru.skedy.Model.Teachers;

public class GroupListActivity extends AppCompatActivity {
    ListView groupsListView;
    TextView helloText;
    DownloadGroups groups;
    SkedyApi skedyApi;
    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = Room.databaseBuilder(GroupListActivity.this, AppDatabase.class, "skedydb").allowMainThreadQueries().build();
        skedyApi = ApiFactory.getSkedyApi();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(view);
//                groupsAndTeachers = new DownloadGroups(skedyApi, db);
//                groupsAndTeachers.execute();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });
        final String[] groups;

        GroupsOnDB[] groupsOnDB = db.gerGONDDao().getAllGroups();
        if(groupsOnDB.length > 0){
            groups = new String[groupsOnDB.length];
            for(int i = 0; i < groupsOnDB.length; i++){
                groups[i] = groupsOnDB[i].getName();
            }
        }else groups = new String[]{};

        groupsListView = findViewById(R.id.groups_list_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.groups_list_element, groups);
        groupsListView.setAdapter(adapter);

        groupsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent i1 = new Intent(GroupListActivity.this, SlidingTabActivity.class);
                startActivity(i1);
            }
        });

        helloText = findViewById(R.id.hello_groups_text);
        if(groupsListView.getCount() != 0){
            helloText.setVisibility(View.INVISIBLE);
        }

    }


    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popupmenu); // Для Android 4.0
        // для версии Android 3.0 нужно использовать длинный вариант
        // popupMenu.getMenuInflater().inflate(R.menu.popupmenu,
        // popupMenu.getMenu());

        popupMenu
                .setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Toast.makeText(PopupMenuDemoActivity.this,
                        // item.toString(), Toast.LENGTH_LONG).show();
                        // return true;
                        switch (item.getItemId()) {

                            case R.id.menu1:
                                DownloadGroups downloadGroups = new DownloadGroups(skedyApi, db);
                                downloadGroups.execute();
                                return true;
                            case R.id.menu2:
                                DownloadTeachers downloadTeachers = new DownloadTeachers(skedyApi, db);
                                downloadTeachers.execute();
                                return true;
                            default:
                                return false;
                        }
                    }
                });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu menu) {

            }
        });
        popupMenu.show();
    }


    private class DownloadGroups extends AsyncTask<Void, Void, Void>{
        SkedyApi api;
        AppDatabase db;

        public DownloadGroups(SkedyApi api, AppDatabase db) {
            this.api = api;
            this.db = db;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while (!getGroups(api, db)){
                Log.i("err", "err");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent i = new Intent(GroupListActivity.this, AddScheduleActivity.class);
            i.putExtra("flag", "g");
            startActivity(i);
        }
    }

    private class DownloadTeachers extends AsyncTask<Void, Void, Void>{
        SkedyApi api;
        AppDatabase db;

        public DownloadTeachers(SkedyApi api, AppDatabase db) {
            this.api = api;
            this.db = db;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while (!getTeachers(api, db)){
                Log.i("err", "err");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent i = new Intent(GroupListActivity.this, AddScheduleActivity.class);
            i.putExtra("flag", "t");
            startActivity(i);
        }
    }

    private boolean getGroups(SkedyApi api, AppDatabase db){
        try {
            Response response = api.getGroups().execute();
            if(response.isSuccessful()){
                db.getGroupsDao().nukeTable();
                ArrayList<Groups> list = (ArrayList<Groups>) response.body();
                db.getGroupsDao().insertAll(list.toArray(new Groups[list.size()]));
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private boolean getTeachers(SkedyApi api, AppDatabase db){
        try {
            Response response = api.getTeachers().execute();
            if(response.isSuccessful()){
                ArrayList<Teachers> list = (ArrayList<Teachers>) response.body();
                db.getTeachersDao().addAll(list.toArray(new Teachers[list.size()]));
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
