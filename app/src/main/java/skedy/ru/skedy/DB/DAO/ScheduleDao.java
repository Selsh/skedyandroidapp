package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import skedy.ru.skedy.Model.Schedule;

/**
 * Created by fabvest on 29.10.2017.
 */
@Dao
public interface ScheduleDao {
    @Insert
    void addAll(Schedule... schedules);

    @Query("SELECT * FROM schedule")
    Schedule[] getAllSchedule();
}
