package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import skedy.ru.skedy.Model.Teachers;

@Dao
public interface GATDao {
    @Insert
    void insertAll(Teachers... teacherses);

    @Query("SELECT * FROM teachers")
    List<Teachers> getAllGAT();
}
