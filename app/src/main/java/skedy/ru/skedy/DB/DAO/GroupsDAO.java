package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import skedy.ru.skedy.Model.Groups;

@Dao
public interface GroupsDAO {
    @Insert
    void insertAll(Groups... groups);

    @Insert
    void insertOne(Groups groups);

    @Query("SELECT * FROM groups")
    Groups[] getAllGroups();

    @Query("DELETE FROM groups")
    void nukeTable();
}
