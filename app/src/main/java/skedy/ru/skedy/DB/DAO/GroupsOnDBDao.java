package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import skedy.ru.skedy.Model.GroupsOnDB;

@Dao
public interface GroupsOnDBDao {
    @Insert
    void addAll(GroupsOnDB... groupsOnDBs);

    @Insert
    void addOne(GroupsOnDB groupsOnDB);

    @Query("SELECT * FROM groupondb")
    GroupsOnDB[] getAllGroups();
}
