package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import skedy.ru.skedy.Model.Teachers;

/**
 * Created by fabvest on 29.10.2017.
 */
@Dao
public interface TeachersDao {
    @Insert
    void addAll(Teachers... teachers);

    @Insert
    void addOne(Teachers teachers);

    @Query("SELECT * FROM teachers")
    Teachers[] getAllTeachers();
}
