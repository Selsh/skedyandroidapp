package skedy.ru.skedy.DB.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import skedy.ru.skedy.Model.TeachersOnDB;

/**
 * Created by fabvest on 29.10.2017.
 */
@Dao
public interface TeachOnDB {
    @Insert
    void addAll(TeachersOnDB... teachOnDBs);

    @Insert
    void addOne(TeachersOnDB teachOnDB);
}
