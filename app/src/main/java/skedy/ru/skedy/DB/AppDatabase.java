package skedy.ru.skedy.DB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import skedy.ru.skedy.DB.DAO.GATDao;
import skedy.ru.skedy.DB.DAO.GroupsDAO;
import skedy.ru.skedy.DB.DAO.GroupsOnDBDao;
import skedy.ru.skedy.DB.DAO.ScheduleDao;
import skedy.ru.skedy.DB.DAO.TeachOnDB;
import skedy.ru.skedy.DB.DAO.TeachersDao;
import skedy.ru.skedy.Model.Groups;
import skedy.ru.skedy.Model.GroupsOnDB;
import skedy.ru.skedy.Model.Schedule;
import skedy.ru.skedy.Model.Teachers;
import skedy.ru.skedy.Model.TeachersOnDB;

@Database(entities = {Teachers.class, Groups.class, GroupsOnDB.class,
        TeachersOnDB.class, Schedule.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract GATDao getGATDao();
    public abstract GroupsDAO getGroupsDao();
    public abstract GroupsOnDBDao gerGONDDao();
    public abstract TeachOnDB getTeachOnDBDao();
    public abstract TeachersDao getTeachersDao();
    public abstract ScheduleDao getScheduleDao();
}
