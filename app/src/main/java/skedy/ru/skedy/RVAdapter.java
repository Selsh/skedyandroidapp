package skedy.ru.skedy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import skedy.ru.skedy.Model.Schedule;


public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder> {
    private Schedule[] schedules;
    Context context;

    public RVAdapter(Schedule[] schedules) {
        this.schedules = schedules;
    }

    @Override
    public RVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_list_view, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RVAdapter.ViewHolder holder, int position) {
        context = holder.itemView.getContext();
        holder.lesson.setText(schedules[position].getLessonName());
        Log.i("some", "onBindViewHolder: " + schedules[position].getLessonName());
        holder.time.setText(schedules[position].getTime());
        holder.type.setText(schedules[position].getLessonType());
        holder.aud.setText(schedules[position].getHallNumber());
        holder.teacher.setText(schedules[position].getTeacher());
    }

    @Override
    public int getItemCount() {
        return schedules.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView lesson;
        public TextView time;
        public TextView type;
        public TextView aud;
        public TextView teacher;


        public ViewHolder(View itemView) {
            super(itemView);
            lesson = itemView.findViewById(R.id.lesson_text_view);
            time = itemView.findViewById(R.id.time_text_view);
            type = itemView.findViewById(R.id.type_text_view);
            aud = itemView.findViewById(R.id.class_text_veiw);
            teacher = itemView.findViewById(R.id.teachname_text_view);
        }
    }

}
