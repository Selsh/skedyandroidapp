package skedy.ru.skedy.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import skedy.ru.skedy.Model.Groups;
import skedy.ru.skedy.Model.Schedule;
import skedy.ru.skedy.Model.Teachers;

/**
 * Created by fabvest on 27.10.2017.
 */

public interface SkedyApi{
    @GET("getTeachers")
    Call<List<Teachers>> getTeachers();

    @GET("getGroups")
    Call<List<Groups>> getGroups();

    @GET("getScheduleByGroup/{number}/")
    Call<List<Schedule>> getSchedule(@Path("number") Integer number);

}
