package skedy.ru.skedy.Api;


import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiFactory {
    private static final OkHttpClient CLIENT = new OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true).build();

    @NonNull
    public static SkedyApi getSkedyApi(){
        return getRetrofit().create(SkedyApi.class);
    }

    @NonNull
    private static Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("http://37.230.113.79:8082/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(CLIENT)
                .build();
    }
}
