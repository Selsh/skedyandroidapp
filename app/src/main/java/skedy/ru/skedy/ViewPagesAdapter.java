package skedy.ru.skedy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by fabvest on 28.10.2017.
 */

public class ViewPagesAdapter extends FragmentStatePagerAdapter {
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int numbOfTabs;
    Tab[] tabs;

    public ViewPagesAdapter(FragmentManager fm, CharSequence[] titles, int numbOfTabs) {
        super(fm);
        Titles = titles;
        this.numbOfTabs = numbOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public Fragment getItem(int position) {
        tabs = new Tab[numbOfTabs];
        for(int i = 0; i < numbOfTabs; i++){
            tabs[i] = new Tab();
            tabs[i].setDay(1);
            return tabs[i];
        }
        return null;
    }

    @Override
    public int getCount() {
        return numbOfTabs;
    }
}
