package skedy.ru.skedy;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import skedy.ru.skedy.DB.AppDatabase;
import skedy.ru.skedy.Model.Schedule;


public class Tab extends android.support.v4.app.Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<Schedule> schedules;
    AppDatabase db;
    int day;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);

        db = Room.databaseBuilder(getActivity(), AppDatabase.class, "skedydb").allowMainThreadQueries().build();

        Schedule[] schedules = db.getScheduleDao().getAllSchedule();


//        for(int i = 0; i < 10; i++){
//            schedules1[i] = new Schedule("2-202" + i, "1" + i, "12:00" +i, "Пискаев Кирилл Юрьевич" + i, "Лекция" + i, "Информационные системы" + i);
//        }
//        int count =0;
//        for(int i = 0; i < schedules.length; i++){
//            if(schedules[i].getDayOfTheWeak() == String.valueOf(day)){
//                count++;
//            }
//        }
//        Schedule[] schedules1 = new Schedule[count];
//
//
//        for(int i = 0; i < count; i++){
//            if(schedules[i].getDayOfTheWeak() == String.valueOf(day)){
//                schedules1[i] = schedules[i];
//            }
//        }
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        adapter = new RVAdapter(schedules);
        recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }
}
