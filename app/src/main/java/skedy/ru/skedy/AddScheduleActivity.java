package skedy.ru.skedy;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Response;
import skedy.ru.skedy.Api.ApiFactory;
import skedy.ru.skedy.Api.SkedyApi;
import skedy.ru.skedy.DB.AppDatabase;
import skedy.ru.skedy.Model.Groups;
import skedy.ru.skedy.Model.GroupsOnDB;
import skedy.ru.skedy.Model.Schedule;
import skedy.ru.skedy.Model.Teachers;

public class AddScheduleActivity extends AppCompatActivity {
    Button addSchedule;
    AutoCompleteTextView autoCompleteTextView;
    AppDatabase db;
    Teachers[] teachers;
    Groups[] groups;
    SkedyApi api;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);

        api = ApiFactory.getSkedyApi();

        addSchedule = findViewById(R.id.add_schedule_button);
        addSchedule.setClickable(false);
        addSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(autoCompleteTextView.getText() != null){
                    String needStr = autoCompleteTextView.getText().toString();

                    for(int i = 0; i < groups.length; i++){
                        if(Objects.equals(groups[i].getName(), needStr)){
                            GroupsOnDB gr = new GroupsOnDB();
                            gr.setName(groups[i].getName());
                            db.gerGONDDao().addOne(gr);
                            Log.i("number is", "onClick: " + groups[i].getId_());
                            DownloadGroup downloadGroup = new DownloadGroup(api, db, groups[i].getId_());
                            downloadGroup.execute();
                        }
                    }
                }
            }
        });

        db = Room.databaseBuilder(AddScheduleActivity.this, AppDatabase.class, "skedydb").allowMainThreadQueries().build();

        Intent newIntent = getIntent();

        String flag = newIntent.getStringExtra("flag");

        final String[] array;

        if (Objects.equals(flag, "t")) {
            teachers = db.getTeachersDao().getAllTeachers();
            if (teachers.length > 0) {
                array = new String[teachers.length];
                for (int i = 0; i < teachers.length; i++) {
                    array[i] = teachers[i].getName();
                    Log.i("Teacher", "onCreate: " + teachers[i].getName());
                }
            } else array = new String[]{};
        } else if (Objects.equals(flag, "g")) {
            groups = db.getGroupsDao().getAllGroups();
            if (groups.length > 0) {
                array = new String[groups.length];
                for (int i = 0; i < groups.length; i++) {
                    array[i] = groups[i].getName();
                    Log.i("Groups", "onCreate: " + groups[i].getName());
                }
            } else array = new String[]{};
        } else array = new String[]{};


        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, array);
        autoCompleteTextView.setAdapter(arrayAdapter);



    }

    private class DownloadGroup extends AsyncTask<Void, Void, Void> {
        SkedyApi api;
        AppDatabase db;
        Integer number;

        public DownloadGroup(SkedyApi api, AppDatabase db, Integer number) {
            this.api = api;
            this.db = db;
            this.number = number;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while (!getShedule(api, db, number)) {
                Log.i("schedule", "doInBackground: err");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent i = new Intent(AddScheduleActivity.this, GroupListActivity.class);
            startActivity(i);
        }
    }

    private boolean getShedule(SkedyApi api, AppDatabase db, Integer number) {
        Response response = null;
        try {
            response = api.getSchedule(number).execute();
            if (response.isSuccessful()) {
                ArrayList<Schedule> list = (ArrayList<Schedule>) response.body();
                Log.i("response", response.toString());
                Log.i("listsize", String.valueOf(list.size()));
                for(Schedule s : list){
                    Log.i("schedule", s.getDayOfTheWeak() + s.getGroup() + s.getHallNumber() + s.getLessonName() +s.getLessonType()+ s.getTeacher() +s.getTime());
                }
                db.getScheduleDao().addAll(list.toArray(new Schedule[list.size()]));
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
